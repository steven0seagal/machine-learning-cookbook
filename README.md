# Machine learning-cookbook

Supervised Learning

Linear Regression
Logistic Regression
Decision Trees
Random Forests
Support Vector Machines (SVM)
Naive Bayes Classifier
K-Nearest Neighbors (KNN)
Gradient Boosting Machines (e.g., XGBoost, LightGBM, CatBoost)
Neural Networks
Perceptron
Ridge/Lasso Regression
Unsupervised Learning

K-Means Clustering
Hierarchical Clustering
Principal Component Analysis (PCA)
Singular Value Decomposition (SVD)
Independent Component Analysis (ICA)
t-Distributed Stochastic Neighbor Embedding (t-SNE)
Apriori Algorithm for Association Rule Learning
DBSCAN (Density-Based Spatial Clustering of Applications with Noise)
Gaussian Mixture Models
Self-Organizing Maps
Semi-Supervised Learning

Label Propagation
Semi-Supervised Support Vector Machines
Reinforcement Learning

Q-Learning
SARSA (State-Action-Reward-State-Action)
Deep Q Network (DQN)
Monte Carlo Methods
Temporal Difference Methods
Policy Gradient Methods
Actor-Critic Methods
A3C (Asynchronous Advantage Actor-Critic)
Deep Learning

Convolutional Neural Networks (CNN)
Recurrent Neural Networks (RNN)
Long Short-Term Memory Networks (LSTM)
Gated Recurrent Units (GRU)
Autoencoders
Variational Autoencoders (VAE)
Generative Adversarial Networks (GAN)
Transformer Networks
Deep Belief Networks (DBN)
Restricted Boltzmann Machines (RBM)
Ensemble Methods

Bagging (e.g., Bootstrap Aggregating)
Boosting
Stacking
Other Methods

Anomaly Detection
Feature Selection Techniques
Dimensionality Reduction Techniques
Transfer Learning
Natural Language Processing (NLP) Methods
Computer Vision Techniques
Time Series Forecasting
Recommender Systems