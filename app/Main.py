import streamlit as st
import json

def get_names():
    with open('/usr/src/app/data/tags.json') as f:
        data = json.load(f)
    return data.keys()

def main():
    # Set up the main pages
    st.set_page_config(page_title="Biostatistics Data",
                       page_icon="🧬",

                       )
    st.title("Biostatistics Data Dashboard")


    # Path to the additional logo image
    # sidebar_logo_path = "path/to/sidebar/logo.png"  # Replace with your logo path

    # HTML for displaying the logo in the sidebar
    # sidebar_logo_html = f"<img src='{sidebar_logo_path}' style='max-width: 100%; margin-bottom: 20px;'>"

    # Display the logo in the sidebar using markdown with unsafe HTML
    # st.sidebar.markdown(sidebar_logo_html, unsafe_allow_html=True)

    # Additional sidebar elements can go here
    # st.sidebar.header("Statistical tests")
    # Add navigation or other sidebar widgets below

    # Main pages content
    # st.write("""
    #     Welcome to the Biostatistics Data Dashboard. This platform provides insights and data
    #     analysis in the field of biostatistics. Explore statistical data related to biology,
    #     medicine, and public health.
    # """)

    # st.subheader("Statistical tests available here")
    #
    # names = get_names()
    # names = list(names)
    #
    # for name in names:
    #     st.write(name)

if __name__ == "__main__":
    main()

